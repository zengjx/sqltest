#  1.牛客网

 **SQL1** **查找最晚入职员工的所有信息**

 

```
https://www.nowcoder.com/practice/218ae58dfdcd4af195fff264e062138f?tpId=82&tags=&title=&difficulty=0&judgeStatus=0&rp=1
```

```
drop table if exists  `employees` ; 
CREATE TABLE `employees` (
`emp_no` int(11) NOT NULL,
`birth_date` date NOT NULL,
`first_name` varchar(14) NOT NULL,
`last_name` varchar(16) NOT NULL,
`gender` char(1) NOT NULL,
`hire_date` date NOT NULL,
PRIMARY KEY (`emp_no`));
INSERT INTO employees VALUES(10001,'1953-09-02','Georgi','Facello','M','1986-06-26');
INSERT INTO employees VALUES(10002,'1964-06-02','Bezalel','Simmel','F','1985-11-21');
INSERT INTO employees VALUES(10003,'1959-12-03','Parto','Bamford','M','1986-08-28');
INSERT INTO employees VALUES(10004,'1954-05-01','Chirstian','Koblick','M','1986-12-01');
INSERT INTO employees VALUES(10005,'1955-01-21','Kyoichi','Maliniak','M','1989-09-12');
INSERT INTO employees VALUES(10006,'1953-04-20','Anneke','Preusig','F','1989-06-02');
INSERT INTO employees VALUES(10007,'1957-05-23','Tzvetan','Zielinski','F','1989-02-10');
INSERT INTO employees VALUES(10008,'1958-02-19','Saniya','Kalloufi','M','1994-09-15');
INSERT INTO employees VALUES(10009,'1952-04-19','Sumant','Peac','F','1985-02-18');
INSERT INTO employees VALUES(10010,'1963-06-01','Duangkaew','Piveteau','F','1989-08-24');
INSERT INTO employees VALUES(10011,'1953-11-07','Mary','Sluis','F','1990-01-22');

```

  解法1 ：按照入职时间 排序 然后取出第一条

 

```


select * from employees   order by hire_date desc   limit 1 ;
SELECT * FROM employees order by hire_date desc limit 0,1
SELECT * FROM employees WHERE hire_date == (SELECT MAX(hire_date) FROM employees)
```





##    1.2 分数



###  1.2.1 题解 | #考试分数(一)#

题目链接：http://www.nowcoder.com/practice/f41b94b4efce4b76b27dd36433abe398

```
drop table if exists grade;
CREATE TABLE  grade(
`id` int(4) NOT NULL,
`job` varchar(32) NOT NULL,
`score` int(10) NOT NULL,
PRIMARY KEY (`id`));

INSERT INTO grade VALUES
(1,'C++',11001),
(2,'C++',10000),
(3,'C++',9000),
(4,'Java',12000),
(5,'Java',13000),
(6,'JS',12000),
(7,'JS',11000),
(8,'JS',9999);
预期：
输出：
Java|12500.000
JS|10999.667
C++|10000.333

```



题解：

```
先按照job 分组然后统计每个分组的求和除以统计个数、然后保留小数3位。再按照平均分排序
-- select job|avg(1.0*score) from grade  group by job  ;
   select   job ,  round((1.0*sum(score)/count(id) ),3) as a from grade  group by job  order by a desc ;
```



### 1.2.2 考试分数2 

：http://www.nowcoder.com/practice/f456dedf88a64f169aadd648491a27c1

```
drop table if exists grade;
CREATE TABLE  grade(
`id` int(4) NOT NULL,
`job` varchar(32) NOT NULL,
`score` int(10) NOT NULL,
PRIMARY KEY (`id`));

INSERT INTO grade VALUES
(1,'C++',11001),
(2,'C++',10000),
(3,'C++',9000),
(4,'Java',12000),
(5,'Java',13000),
(6,'JS',12000),
(7,'JS',11000),
(8,'JS',9999),
(9,'Java',12500);
输出：
1|C++|11001
5|Java|13000
6|JS|12000
7|JS|11000
```



 写一个sql语句查询用户分数大于其所在工作(job)分数的平均分的所有grade的属性，并且以id的升序排序 

```
   -- select job|avg(1.0*score) from grade  group by job  ;
      select   job ,  round((1.0*sum(score)/count(id) ),3) as a from grade  group by job  order by a desc ;

```

```

```

# mysql 分组查询取各分组的前两名及子查询性能优化

https://blog.csdn.net/sinat_41780498/article/details/79956561

背景 求出运动员前两个比赛记录

 如何找出每个程序最近的日志条目？如何找出每个用户的最高分？在每个分类中最受欢迎的商品是什么?通常这类“找出每个分组中最高分的条目”的问题可以使用相同的技术来解决。在这篇文章里我将介绍如何解决这类问题，而且会介绍如何找出最高的前几名而不仅仅是第一名。 

https://www.cnblogs.com/zhuiluoyu/p/6862547.html



### 1.2.3 考试分数3  重要

[考试分数3]: https://www.nowcoder.com/practice/b83f8b0e7e934d95a56c24f047260d91?tpId=82&amp;tqId=35493&amp;rp=1&amp;ru=%2Fta%2Fsql&amp;qru=%2Fta%2Fsql%2Fquestion-ranking



 **找出每个岗位分数排名前2名的用户，得到的结果先按照language的name升序排序，再按照积分降序排序，最后按照grade的id升序排序** 

```
drop table if exists grade;
drop table if exists language;
CREATE TABLE `grade` (
`id` int(4) NOT NULL,
`language_id` int(4) NOT NULL,
`score` int(4) NOT NULL,
PRIMARY KEY (`id`));

CREATE TABLE `language` (
`id` int(4) NOT NULL,
`name` varchar(32) NOT NULL,
PRIMARY KEY (`id`));

INSERT INTO grade VALUES
(1,1,12000),
(2,1,13000),
(3,2,11000),
(4,2,10000),
(5,3,11000),
(6,1,11000),
(7,2,11000);

INSERT INTO language VALUES
(1,'C++'),
(2,'JAVA'),
(3,'Python');
```

```
2|C++|13000
1|C++|12000
3|JAVA|11000
7|JAVA|11000
4|JAVA|10000
5|Python|11000
```

 **核心思路:** 将当前表分数跟一个临时表进行比较，当临时表没有一个分数，或者只有一个分数比当前表高时，说明该分数排名是第一名，或者第二名。
**举例：**只看C++语言，id为1时，12000<13000，说明这是第二名，id为2时，13000统计出来为0个，没人比他小，说明他是第一名，id为6时，可以知道11000小于12000和13000，为2，说明是第三名。
**避坑点：**一定要去重！不然在java语言的时候，10000小于两个12000，就是第三名了，然而题目要求可以并列，为第二名   

```

      select   g1.id ,la.name ,g1.score  from  grade  g1    ,language la where
        g1.language_id = la.id  and 
         ( select count( distinct  g2.score) from grade g2 where  g1.language_id=g2.language_id 
         and g1.score < g2.score)  in (0,1)  order by la.name  asc,   g1.score desc ,g1.id asc ;
  
```



###  **SQL75** **考试分数(四)**    分组分数中位数 较难

![](.\sqlimg\sql75.png)



```
drop table if exists grade;
CREATE TABLE  grade(
`id` int(4) NOT NULL,
`job` varchar(32) NOT NULL,
`score` int(10) NOT NULL,
PRIMARY KEY (`id`));

INSERT INTO grade VALUES
(1,'C++',11001),
(2,'C++',10000),
(3,'C++',9000),
(4,'Java',12000),
(5,'Java',13000),
(6,'B',12000),
(7,'B',11000),
(8,'B',9999);
```

解法1 ： 

```
  select  job, FLOOR((count(*)+1)/2)  as "start" ,CEIL((count(*)+1)/2) as "end" 
  from  grade   group by job  order by  job ;
  
   
  select  job, FLOOR((count(*)+1)/2)  as "start" ,floor((count(*)+2)/2) as "end" 
  from  grade   group by job  order by  job ;
```



解法2 ： 

case  when ： 

```
 select job,
 case  when   count(score)%2=0  then floor(count(score)/2) 
 else floor( count(score)/2 +1)  end  as "start", 
 case  when  count(score)%2 =0 then floor(count(score)/2)+1 else floor(count(score)/2)+1
 end  as "end" 
 from   grade   group by job order by  job ;
```



###   **SQL76** **考试分数(五)** 



